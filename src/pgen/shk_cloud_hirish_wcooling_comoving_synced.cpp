 // C++ headers
#include <cmath>      // sqrt()
#include <iostream>   // endl
#include <sstream>    // stringstream
#include <stdexcept>  // runtime_error
#include <string>     // c_str()
#include <mpi.h>
#include <stdio.h>

// Athena++ headers
#include "../athena.hpp"
#include "../athena_arrays.hpp"
#include "../parameter_input.hpp"
#include "../bvals/bvals.hpp"
#include "../coordinates/coordinates.hpp"
#include "../eos/eos.hpp"
#include "../field/field.hpp"
#include "../hydro/hydro.hpp"
#include "../mesh/mesh.hpp"
#include "../utils/utils.hpp"

/* STATIC VARIABLES FOR COOLING */
// Cooling Tables
static int nbins;
static Real const_factor;
static AthenaArray<Real> cool_t;
static AthenaArray<Real> cool_tef;
static AthenaArray<Real> cool_coef;
static AthenaArray<Real> cool_index;

// All units are in cgs
static const Real mh        = 1.6605e-24;   // atomic mass unit (g)
static const Real kb        = 1.380648e-16; // boltzmann constant (erg/K)

static const Real unit_len  = 3.086e20;     // 100 pc
static const Real unit_temp = 1.0e6;        // 10^6 K
static const Real unit_n    = 1.6e-4;       // cm^-3

static const Real unit_rho  = unit_n * mh;
static const Real unit_pres = kb * unit_n * unit_temp;
static const Real unit_vel  = sqrt(unit_pres / unit_rho); // ~ 90 km/s
static const Real unit_time = unit_len / unit_vel;        // ~ 1 Myr
static const Real unit_mag  = sqrt(4*PI*unit_pres);       // ~ 0.5 microGauss

static const Real g  = 5.0/3.0; // adiabatic index
static const Real X = 0.7; Real Z = 0.02; // Fully ionized, solar abundances
static const Real mu   = 1.0/(2.0*X + 0.75*(1.0-X-Z) + Z/2.0);
static const Real mu_e = 2.0/(1.0+X);
static const Real mu_h = 1.0/X;

/* STATIC VARIABLES TO HOLD INPUT DATA THAT CAN BE ACESSED GLOBALLY*/
static Real gmma1;
static Real gridPressure, gridVelocity, gridDensity;
static Real rad, cloudPressure, cloudVelocity, cloudDensity;
static Real windVelocity;
static Real cloudCrushingTime; //IN CODE UNITS
static Real tCoolMix;
static Real cloudTemperature;
static Real windTemperature;
static Real t_floor;
static Real speedOfSound;
static Real coolingFloor;
static int callNumber;
static bool shifted;
static Real velocityBoi;
static Real globalMeanCloudVelocity;
static int cycleNumber;
static int boundaryCallNumber;
static bool freePass;
static Real heatingFloor;
static Real grav;
static Real globalColdGasMomentum;
static Real globalColdGasMass;

/* THE FRACTION OF ORIGINAL DENSITY OF COLD CLOUD THAT IS CONSIDERED COLD*/
static Real coldGasFraction = 0.33;

/* This function sets the boundary conditions for left_x1:
*/
void ShockCloudInnerX1(MeshBlock *pmb, Coordinates *pco, AthenaArray<Real> &prim,
                       FaceField &b, Real time, Real dt,
                       int is, int ie, int js, int je, int ks, int ke, int ngh);
void ShockCloudOuterX1(MeshBlock *pmb, Coordinates *pco, AthenaArray<Real> &prim,
                       FaceField &b, Real time, Real dt,
                       int is, int ie, int js, int je, int ks, int ke, int ngh);

/* User Defined Cooling Function:
*/
void TownsendCoolingAndBoosting(MeshBlock *pmb, const Real time, const Real dt, const AthenaArray<Real> &prim,
                     const AthenaArray<Real> &bcc, AthenaArray<Real> &cons);
void Cooling(MeshBlock *pmb, const Real time, const Real dt, const AthenaArray<Real> &prim, const AthenaArray<Real> &bcc,
             AthenaArray<Real> &cons);
/* Another user defined cooling function, shifts the reference frame: 
*/
void BoostFrame(MeshBlock *pmb, const Real time, const Real dt, const AthenaArray<Real> &prim,
                const AthenaArray<Real> &bcc, AthenaArray<Real> &cons);

void GalileanTranformation(MeshBlock *pmb, const Real time, const Real dt,
                     const AthenaArray<Real> &prim, const AthenaArray<Real> &bcc,
                     AthenaArray<Real> &cons);

/* User Defined History Output 1:
*/
Real ColdGasMass(MeshBlock *pmb, int iout);

/* User Defined History Output 2:
*/
Real ColdGasMomentum(MeshBlock *pmb, int iout);

/* Grid Velocity
*/
Real GridVelocityOut(MeshBlock *pmb, int iout);

/* Functions for my convenience, to see if simulation is working right:
*/
double t_cool(double rho, double T);
double Lambda(double T);
double t_cc();

double t_cc()
{
    cloudCrushingTime = sqrt(cloudDensity/gridDensity) * rad/ windVelocity;
    return cloudCrushingTime;
}

double t_cc_cgs()
{
    return sqrt(cloudDensity/gridDensity) * (rad*unit_len)/ (windVelocity*unit_vel);
}


double t_cool(double rho, double T) //takes in rho and T in CODE UNITS!!!! AND OUTPUTS IN CGS!!!
{
    tCoolMix = std::pow(10, 23) * (kb * mu_e * mu_h * mh * (T*unit_temp) / ((g - 1) * (rho*unit_rho) * mu * Lambda((T*unit_temp))));
    return tCoolMix;
}

//outputs in units of 10^(-23)
double Lambda(double T) //takes in temperature in CGS UNITS!!!! MUST BE CGS!!!
{
    int index = 0;
    for (int i = 0; i < 40; i++)
        if (T > cool_t(i))
            index++;
    if (index != 0)
        index -= 1;
    
    return cool_coef(index) * std::pow(T/cool_t(index), cool_index(index));
}


void MeshBlock::ProblemGenerator(ParameterInput *pin) {
    callNumber = 0;

    shifted = false;

    Real gmma  = peos->GetGamma();
    gmma1 = gmma - 1.0;
    
    gridPressure = pin->GetReal("problem", "GP");
    gridVelocity = pin->GetReal("problem", "GV");
    gridDensity = pin->GetReal("problem", "GD");
    rad = pin->GetReal("problem", "CR");
    cloudPressure = pin->GetReal("problem", "CP");
    cloudVelocity = pin->GetReal("problem", "CV");
    cloudDensity = pin->GetReal("problem", "CD");
    coolingFloor = pin->GetReal("problem", "CF");
    heatingFloor = pin->GetReal("problem", "HF");

    speedOfSound = std::sqrt(gmma * gridPressure/gridDensity); //speed of sound is in code units
    
    std::cout << "SPEED OF SOUND: " << speedOfSound << std::endl;
    
    gridVelocity = gridVelocity * speedOfSound;
    cloudVelocity = cloudVelocity * speedOfSound;
    
    windVelocity = gridVelocity; 
    
    std::cout << "CLOUD CRUSHING TIME (IN CODE UNITS): " << t_cc() << std::endl;
    std::cout << "CLOUD DENSITY (CU): " << cloudDensity << std::endl;
    std::cout << "CLOUD PRESSURE (CU): " << cloudPressure << std::endl;
    std::cout << "CLOUD RADIUS (CU): " << rad << std::endl;
    
    //CLOUD TEMPERATURE IS IN CODE UNITS!
    cloudTemperature = cloudPressure/cloudDensity;
    windTemperature = gridPressure/gridDensity;

    //SET t_floor here:
    std::cout << "TEMPERATURE FLOOR" << t_floor << std::endl;

    t_floor = coolingFloor * cloudTemperature * unit_temp;
    
    std::cout << "TEMPERATURE CLOUD (K): " << cloudTemperature * unit_temp << std::endl;
    std::cout << "TEMPERATURE WIND (K): " << windTemperature * unit_temp << std::endl;
    
    std::cout << "CLOUD CRUSHING TIME (IN CGS): " << t_cc_cgs() << std::endl;
    std::cout << "TCOOL,MIX (IN CGS): " << t_cool(std::sqrt(cloudDensity * gridDensity), std::sqrt(cloudTemperature * windTemperature)) << std::endl;
    std::cout << "TCOOL,WIND (IN CGS): " << t_cool(gridDensity, windTemperature) << std::endl;
    std::cout << "LAMBDA(T)" << Lambda(cloudTemperature*unit_temp) << std::endl;
    std::cout << "UNIT TIME: " << unit_time << std::endl;
    std::cout << "TCOOL,MIX/T_CC: " << t_cool(std::sqrt(cloudDensity * gridDensity), std::sqrt(cloudTemperature * windTemperature))/t_cc_cgs() << std::endl;

    
    grav = pin->GetOrAddReal("hydro","grav_acc1",0.0);

    // Initialize the grid
    for (int k=ks; k<=ke; k++) {
        for (int j=js; j<=je; j++) {
            for (int i=is; i<=ie; i++) {
                Real expFactor = exp(grav * pcoord->x1v(i)/windTemperature);
                //Real expFactor = 1;
                
                phydro->u(IDN,k,j,i) = gridDensity*expFactor;
                phydro->u(IM1,k,j,i) = gridVelocity*gridDensity*expFactor;
                phydro->u(IM2,k,j,i) = 0.0;
                phydro->u(IM3,k,j,i) = 0.0;
                phydro->u(IEN,k,j,i) = (gridPressure*expFactor)/(gmma1) + 0.5 * gridDensity * expFactor * (gridVelocity * gridVelocity);
               
                Real diag = std::sqrt(SQR(pcoord->x1v(i)) + SQR(pcoord->x2v(j))
                                      + SQR(pcoord->x3v(k)));
                if (diag < rad) {
                    Real expFactor2 = expFactor;

                    phydro->u(IDN,k,j,i) = cloudDensity * expFactor2;
                    phydro->u(IM1,k,j,i) = cloudVelocity * cloudDensity * expFactor2;
                    phydro->u(IM2,k,j,i) = 0.0;
                    phydro->u(IM3,k,j,i) = 0.0;
                    phydro->u(IEN,k,j,i) = (cloudPressure*expFactor2)/(gmma1) + 0.5 * expFactor2 * cloudDensity * (cloudVelocity*cloudVelocity);
                }
            }}}


    
    velocityBoi = windVelocity;

    globalMeanCloudVelocity = 0;

    cycleNumber = 0;

    boundaryCallNumber = 0;

    freePass = false;

    globalColdGasMomentum = 0;
    globalColdGasMass = 0;

    return;
}

void ShockCloudInnerX1(MeshBlock *pmb, Coordinates *pco, AthenaArray<Real> &prim,
                       FaceField &b, Real time, Real dt,
                       int is, int ie, int js, int je, int ks, int ke, int ngh) {
    boundaryCallNumber++;

    for (int k=ks; k<=ke; ++k) {
        for (int j=js; j<=je; ++j) {
            for (int i=is-ngh; i<=is; ++i) {
                Real expFactor = exp(grav/windTemperature * pmb->pcoord->x1v(i));
                //Real expFactor = 1;

                prim(IDN,k,j,i) = gridDensity * expFactor;
                prim(IVX,k,j,i) = gridVelocity;
                prim(IVY,k,j,i) = 0.0;
                prim(IVZ,k,j,i) = 0.0;
                prim(IPR,k,j,i) = gridPressure * expFactor;
            }
        }
     }
    
    return;
}

void ShockCloudOuterX1(MeshBlock *pmb, Coordinates *pco, AthenaArray<Real> &prim,
                       FaceField &b, Real time, Real dt,
                       int is, int ie, int js, int je, int ks, int ke, int ngh) {
    for (int k=ks; k<=ke; ++k) {
        for (int j=js; j<=je; ++j) {
            for (int i=ie+1; i<=ie+2; ++i) {
                Real expFactor = exp(grav/windTemperature * pmb->pcoord->x1v(i));
                //Real expFactor = 1;

                prim(IDN,k,j,i) = gridDensity * expFactor;
                prim(IVX,k,j,i) = gridVelocity;
                prim(IVY,k,j,i) = 0.0;
                prim(IVZ,k,j,i) = 0.0;
                prim(IPR,k,j,i) = gridPressure * expFactor;
            }
        }
     }
    
    return;
}

// Exact Integration Scheme for Radiative Cooling from Townsend (2009)
// Returns: Temperature(K) at the next timestep after cooling
// Requires: - Input temperature, density, and timestep in cgs units
//           - T < cool_t(nbins-1) and T > cool_t(0)
//           - All cooling slopes are not equal to 1
Real townsend(Real temp, Real rho, const Real dt)
{
    // PRE COOLING FLOOR t_floor is set in CGS UNITS
    
    // Check that temperature is above the cooling floor
    if (temp > t_floor) {
        // Get Reference values from the last bin
        Real t_n    = cool_t(nbins-1);
        Real coef_n = cool_coef(nbins-1);
        
        // Get the index of the right temperature bin
        int idx = 0;
        while ((idx < nbins-2) && (cool_t(idx+1) < temp)) { idx += 1; }
        
        // Look up the corresponding slope and coefficient
        Real t_i   = cool_t(idx);
        Real tef_i = cool_tef(idx);
        Real coef  = cool_coef(idx);
        Real slope = cool_index(idx);
        
        // Compute the Temporal Evolution Function Y(T) (Eq. A5)
        Real sm1 = slope - 1.0;
        Real tef = tef_i + (coef_n/coef)*(t_i/t_n)*(pow(t_i/temp,sm1)-1)/sm1;
        
        // Compute the adjusted TEF for new timestep (Eqn. 26)
        Real tef_adj = tef + rho*coef_n*const_factor*dt/t_n;
        
        // TEF is a strictly decreasing function and new_tef > tef
        // Check if the new TEF falls into a lower bin
        while ((idx > 0) && (tef_adj > cool_tef(idx))) {
            idx -= 1;
            // If so, update slopes and coefficients
            t_i   = cool_t(idx);
            tef_i = cool_tef(idx);
            coef  = cool_coef(idx);
            slope = cool_index(idx);
        }
        
        // Compute the Inverse Temporal Evolution Function Y^{-1}(Y) (Eq. A7)
        Real oms  = 1.0 - slope;
        Real itef = t_i*pow(1-oms*(coef/coef_n)*(t_n/t_i)*(tef_adj-tef_i),1/oms);
        
        // Return the new temperature if it is still above the temperature floor
        return std::max(itef,t_floor);
    }
    else { return t_floor; }
}

void Cooling(MeshBlock *pmb, const Real time, const Real dt,
             const AthenaArray<Real> &prim, const AthenaArray<Real> &bcc,
             AthenaArray<Real> &cons)
{
  Real g = pmb->peos->GetGamma();

  for (int k=pmb->ks; k<=pmb->ke; ++k) {
    for (int j=pmb->js; j<=pmb->je; ++j) {
      for (int i=pmb->is; i<=pmb->ie; ++i) {
        // Need to take density and temperature at time step n from cons, not
        // prim because we do not need intermediate step to calculate the
        // cooling function
        Real rho = cons(IDN,k,j,i);
        Real eint = cons(IEN,k,j,i)
                    - 0.5 *(cons(IM1,k,j,i)*cons(IM1,k,j,i)
                          + cons(IM2,k,j,i)*cons(IM2,k,j,i)
                          + cons(IM3,k,j,i)*cons(IM3,k,j,i))/rho;
        if(MAGNETIC_FIELDS_ENABLED){
             eint -= 0.5 * (bcc(IB1,k,j,i) * bcc(IB1,k,j,i)
                          + bcc(IB2,k,j,i) * bcc(IB2,k,j,i)
                          + bcc(IB3,k,j,i) * bcc(IB3,k,j,i));
        }

        // T = P/rho
        Real temp = eint * (g-1.0)/rho;

        // Calculate new temperature using the Townsend Algorithm
        // The inputs should be given in cgs units
        Real temp_cgs = temp * unit_temp;
        Real rho_cgs  = rho  * unit_rho;
        Real dt_cgs   = dt   * unit_time;
        Real temp_new = townsend(temp_cgs,rho_cgs,dt_cgs)/unit_temp; //temp_new is in code units

        //POST COOLING FLOOR
        if (temp_new < cloudTemperature * coolingFloor)
            temp_new = cloudTemperature * coolingFloor;

        if (temp_new > windTemperature * heatingFloor)
            temp_new = temp;


        // Update energy based on change in temperature
        cons(IEN,k,j,i) += (temp_new - temp) * (rho/(g-1.0));

        // Store the change in energy in a user defined output variable
        pmb->user_out_var(0,k,j,i) = (temp_new - temp) * rho/(g-1.0);
      }
    }
  }

  return;
}



void GalileanTranformation(MeshBlock *pmb, const Real time, const Real dt,
                     const AthenaArray<Real> &prim, const AthenaArray<Real> &bcc,
                     AthenaArray<Real> &cons)
{
    std::cout << "SOURCE" << std::endl;
    if (time > 1 && shifted == false && (boundaryCallNumber %2 == 1 || freePass))
    {
        callNumber++;
        Real shiftVelocity = 0.01;
        freePass = true;


        std::cout << std::endl << std::endl;
        std::cout << "<---------------------------------------->" << std::endl;
        std::cout << "<---------------------------------------->" << std::endl;
        std::cout << "!!!!!!!!!!!!!!!!!!BOOM@"<< time/cloudCrushingTime <<"t_cc!!!!!!!!!!!!!!!!!" << std::endl;
        std::cout << "shiftVelocity: " << shiftVelocity << std::endl;
        std::cout << "<---------------------------------------->" << std::endl;
        std::cout << "<---------------------------------------->" << std::endl;
        std::cout << std::endl << std::endl;
        

        for (int k=pmb->ks; k<=pmb->ke; ++k) {
            for (int j=pmb->js; j<=pmb->je; ++j) {
                for (int i=pmb->is; i<=pmb->ie; ++i) {
                    cons(IEN,k,j,i) -= cons(IM1,k,j,i) * shiftVelocity - 0.5 * shiftVelocity * shiftVelocity * cons(IDN, k, j, i);
                    cons(IM1,k,j,i) -= shiftVelocity * cons(IDN,k,j,i);
                    //prim(IVX,k,j,i) -= meanCloudVelocity;
                }   
            }
        }

        if (callNumber == 2)
            shifted = true;

        if (callNumber == 1)
            gridVelocity -= shiftVelocity;

    }
   
    return;
}

/*
void MeshBlock::UserWorkInLoop()
{
    Real blockColdGasMass = 0;
    Real blockColdGasMomentum = 0;
    for (int k=ks; k<=ke; ++k) {
        for (int j=js; j<=je; ++j) {
            for (int i=is; i<=ie; ++i) {
                if (phydro->u(IDN,k,j,i) > cloudDensity * coldGasFraction) 
                {
                    blockColdGasMomentum += phydro->u(IM1,k,j,i);
                    blockColdGasMass += phydro->u(IDN,k,j,i);
                }
            }
        }
    }
    std::cout << "MESHBLOCK:: globalColdGasMass: " << blockColdGasMass << std::endl;
    std::cout << "MESHBLOCK:: globalColdGasMomentum: " << blockColdGasMomentum << std::endl;
    return;
}
*/

void Mesh::MeshUserWorkInLoop(ParameterInput *pin)
{
#ifdef MPI_PARALLEL
        // Initialize the MPI environment

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    // Get the name of the processor
    char processor_name[MPI_MAX_PROCESSOR_NAME];
    int name_len;
    MPI_Get_processor_name(processor_name, &name_len);

    // Print off a hello world message
    //printf("Hello world from processor %s, rank %d out of %d processors\n",
    //       processor_name, world_rank, world_size);


    MeshBlock *pmb = this->pblock;

    Real localColdGasMomentum = 0;
    Real localColdGasMass = 0;

    while (pmb != nullptr) {
        for (int k=pmb->ks; k<=pmb->ke; ++k) {
            for (int j=pmb->js; j<=pmb->je; ++j) {
                for (int i=pmb->is; i<=pmb->ie; ++i) {
                    if (pmb->phydro->u(IDN,k,j,i) > cloudDensity * coldGasFraction) 
                    {
                        localColdGasMomentum += pmb->phydro->u(IM1,k,j,i);
                        localColdGasMass += pmb->phydro->u(IDN,k,j,i);
                    }
                }
            }
        }
        pmb = pmb->next;
    }
    //std::cout << "localColdGasMass" << localColdGasMass << std::endl;

    //MPI_Barrier(MPI_COMM_WORLD);

    MPI_Allreduce(&localColdGasMass, &globalColdGasMass, 1, MPI_ATHENA_REAL, MPI_SUM,
              MPI_COMM_WORLD);
    MPI_Allreduce(&localColdGasMomentum, &globalColdGasMomentum, 1, MPI_ATHENA_REAL, MPI_SUM,
              MPI_COMM_WORLD);
    //std::cout << "globalColdGasMass: " << globalColdGasMass << std::endl;


#endif

    Real meanCloudVelocity;

#ifndef MPI_PARALLEL
        MeshBlock *pmb = this->pblock;

        while (pmb != nullptr) {
            for (int k=pmb->ks; k<=pmb->ke; ++k) {
                for (int j=pmb->js; j<=pmb->je; ++j) {
                    for (int i=pmb->is; i<=pmb->ie; ++i) {
                        if (pmb->phydro->u(IDN,k,j,i) > cloudDensity * coldGasFraction) 
                        {
                            globalColdGasMomentum += pmb->phydro->u(IM1,k,j,i);
                            globalColdGasMass += pmb->phydro->u(IDN,k,j,i);
                        }
                    }
                }
            }
            pmb = pmb->next;
        }
#endif

    if (globalColdGasMass == 0)
    {
        meanCloudVelocity = 0;
    }
    else
    {
        meanCloudVelocity = globalColdGasMomentum/globalColdGasMass;
    }


    //std::cout << "MeanCloudVelocity: " << meanCloudVelocity << std::endl;
    if (meanCloudVelocity > 0)
    {
        Real floor = std::min(std::abs(windVelocity/100), std::abs(gridVelocity/100));
        if (std::abs(meanCloudVelocity) > floor)
            meanCloudVelocity = std::abs(floor);
    }
    if (meanCloudVelocity < 0)
    {
        Real floor = std::min(std::abs(windVelocity/100), std::abs(gridVelocity/100));
        if (std::abs(meanCloudVelocity) > floor)
            meanCloudVelocity = -std::abs(floor);
    }

    globalMeanCloudVelocity = meanCloudVelocity;
    //std::cout << "----------------------USERWORKINLOOP-----------------------" << std::endl;
    //std::cout << "coldGasMass: " << globalColdGasMass << std::endl;
    //std::cout << "coldGasMomentum: " << globalColdGasMomentum << std::endl;

    //std::cout << "gridVelocityB4: " << gridVelocity << std::endl;

    gridVelocity -= globalMeanCloudVelocity; //THIS IS AN IMPORTANT LINE OF CODE, COMMENT IT OUT IF BOOSTING IS NOT ON

    //std::cout << "globalMeanCloudVelocity: " << globalMeanCloudVelocity << std::endl;
    //std::cout << "gridVelocityA4: " << gridVelocity << std::endl;
    //std::cout << "-----------------------------------------------------------" << std::endl;
    


	return;
}



// User Defined Cooling Function
//COOLING AND BOOSTING!!!!!!!!!!!!!!!!!!
void TownsendCoolingAndBoosting(MeshBlock *pmb, const Real time, const Real dt,
                     const AthenaArray<Real> &prim, const AthenaArray<Real> &bcc,
                     AthenaArray<Real> &cons)
{
    Real g = pmb->peos->GetGamma();

    
    if (std::abs(globalMeanCloudVelocity) > dt/cloudCrushingTime)
    {
        if (globalMeanCloudVelocity > 0)
        {
            globalMeanCloudVelocity = dt * cloudCrushingTime;
        }
        else
        {
            globalMeanCloudVelocity = -dt * cloudCrushingTime;
        }

    }
    

    //std::cout << "-----------------BOOSTING------------------" << std::endl;
    //std::cout << "globalMeanCloudVelocity: " << globalMeanCloudVelocity << std::endl;
    //std::cout << "-------------------------------------------" << std::endl;


    for (int k=pmb->ks; k<=pmb->ke; ++k) {
        for (int j=pmb->js; j<=pmb->je; ++j) {
            for (int i=pmb->is; i<=pmb->ie; ++i) {

                cons(IEN,k,j,i) -= cons(IM1,k,j,i) * globalMeanCloudVelocity - 0.5 * globalMeanCloudVelocity * globalMeanCloudVelocity * cons(IDN, k, j, i);
                cons(IM1,k,j,i) -= globalMeanCloudVelocity * cons(IDN,k,j,i);

                // Need to take density and temperature at time step n from cons, not
                // prim because we do not need intermediate step to calculate the
                // cooling function
                Real rho = cons(IDN,k,j,i);
                Real eint = cons(IEN,k,j,i)
                - 0.5 *(cons(IM1,k,j,i)*cons(IM1,k,j,i)
                        + cons(IM2,k,j,i)*cons(IM2,k,j,i)
                        + cons(IM3,k,j,i)*cons(IM3,k,j,i))/rho;
                if(MAGNETIC_FIELDS_ENABLED){
                    eint -= 0.5 * (bcc(IB1,k,j,i) * bcc(IB1,k,j,i)
                                   + bcc(IB2,k,j,i) * bcc(IB2,k,j,i)
                                   + bcc(IB3,k,j,i) * bcc(IB3,k,j,i));
                }
                
                // T = P/rho
                Real temp = eint * (g-1.0)/rho;
                
                // Calculate new temperature using the Townsend Algorithm
                // The inputs should be given in cgs units
                Real temp_cgs = temp * unit_temp;
                Real rho_cgs  = rho  * unit_rho;
                Real dt_cgs   = dt   * unit_time;
                Real temp_new = townsend(temp_cgs,rho_cgs,dt_cgs)/unit_temp;

                        //POST COOLING FLOOR
                if (temp_new < cloudTemperature * coolingFloor)
                    temp_new = cloudTemperature * coolingFloor;


                if (temp_new > windTemperature * heatingFloor)
                    temp_new = temp;

                
                // Update energy based on change in temperature
                cons(IEN,k,j,i) += (temp_new - temp) * (rho/(g-1.0));
                
                // Store the change in energy in a user defined output variable
                pmb->user_out_var(0,k,j,i) = (temp_new - temp) * rho/(g-1.0);

                //if (prim(IDN,k,j,i) > cloudDensity * coldGasFraction)
                //{
                //    xSUM += pmb->pcoord->x1v(i) * prim(IDN,k,j,i);
                //    coldGasMass += prim(IDN,k,j,i);
                //}
            }
        }
    }

    return;
}

// Register our user defined cooling function with Athena
void Mesh::InitUserMeshData(ParameterInput *pin)
{
    AllocateUserHistoryOutput(3);
    EnrollUserHistoryOutput(0, ColdGasMass, "ColdGasMass");
    EnrollUserHistoryOutput(1, ColdGasMomentum, "ColdMomentum");
    EnrollUserHistoryOutput(2, GridVelocityOut, "GridVelocity");

    


    //COOLING ONLY IS ON

    //EnrollUserExplicitSourceFunction(Cooling);

    //EnrollUserExplicitSourceFunction(GalileanTranformation);

    //EnrollUserExplicitSourceFunction(TownsendCoolingAndBoosting);
    
    EnrollUserBoundaryFunction(INNER_X1, ShockCloudInnerX1);
    EnrollUserBoundaryFunction(OUTER_X1, ShockCloudOuterX1);
    
    //BOOSTING ON

    //EnrollUserExplicitSourceFunction(BoostFrame);
    
    // Initialize all values in the cooling table
    nbins = 40;
    cool_t.NewAthenaArray(nbins);
    cool_tef.NewAthenaArray(nbins);
    cool_coef.NewAthenaArray(nbins);
    cool_index.NewAthenaArray(nbins);
    
    // Last Temperature should be the ceiling temperature
    // [K]
    cool_t(0) = 10000.0;
    cool_t(1) = 12600.0;
    cool_t(2) = 15890.0;
    cool_t(3) = 20020.0;
    cool_t(4) = 25240.0;
    cool_t(5) = 31810.0;
    cool_t(6) = 40090.0;
    cool_t(7) = 50530.0;
    cool_t(8) = 63680.0;
    cool_t(9) = 80260.0;
    cool_t(10) = 101200.0;
    cool_t(11) = 127500.0;
    cool_t(12) = 160700.0;
    cool_t(13) = 202600.0;
    cool_t(14) = 255300.0;
    cool_t(15) = 321800.0;
    cool_t(16) = 405600.0;
    cool_t(17) = 511100.0;
    cool_t(18) = 644200.0;
    cool_t(19) = 812000.0;
    cool_t(20) = 1000000.0;
    cool_t(21) = 1259000.0;
    cool_t(22) = 1585000.0;
    cool_t(23) = 1995000.0;
    cool_t(24) = 2512000.0;
    cool_t(25) = 3162000.0;
    cool_t(26) = 3981000.0;
    cool_t(27) = 5012000.0;
    cool_t(28) = 6310000.0;
    cool_t(29) = 7943000.0;
    cool_t(30) = 10000000.0;
    cool_t(31) = 12590000.0;
    cool_t(32) = 15850000.0;
    cool_t(33) = 19950000.0;
    cool_t(34) = 25120000.0;
    cool_t(35) = 31620000.0;
    cool_t(36) = 39810000.0;
    cool_t(37) = 50120000.0;
    cool_t(38) = 63100000.0;
    cool_t(39) = 79430000.0;
    
    // [1e-23 ergs*cm3/s]
    cool_coef(0) = 1.6408984689285624;
    cool_coef(1) = 5.789646575948292;
    cool_coef(2) = 18.797203755396648;
    cool_coef(3) = 16.7384754689852;
    cool_coef(4) = 11.274384717759935;
    cool_coef(5) = 9.95038422958871;
    cool_coef(6) = 11.302144847043829;
    cool_coef(7) = 15.819149070534786;
    cool_coef(8) = 25.224636283348048;
    cool_coef(9) = 38.02107089248533;
    cool_coef(10) = 43.98219098299675;
    cool_coef(11) = 41.277704007796586;
    cool_coef(12) = 41.95311185975414;
    cool_coef(13) = 45.260670345801;
    cool_coef(14) = 47.275626188961176;
    cool_coef(15) = 32.21420131907784;
    cool_coef(16) = 24.350976818250636;
    cool_coef(17) = 23.383616480583676;
    cool_coef(18) = 18.333394532081098;
    cool_coef(19) = 14.89691888284402;
    cool_coef(20) = 14.392505898454834;
    cool_coef(21) = 13.027915287005817;
    cool_coef(22) = 11.671262753284271;
    cool_coef(23) = 9.070904785425046;
    cool_coef(24) = 6.489695397654223;
    cool_coef(25) = 4.766239129792971;
    cool_coef(26) = 3.7811870710765074;
    cool_coef(27) = 3.313622783657129;
    cool_coef(28) = 3.0600313080475674;
    cool_coef(29) = 2.9993768457216112;
    cool_coef(30) = 2.9491332141250552;
    cool_coef(31) = 2.744653611808266;
    cool_coef(32) = 2.3449511265716;
    cool_coef(33) = 2.0169621177549892;
    cool_coef(34) = 1.8907205849384978;
    cool_coef(35) = 1.91584885606706;
    cool_coef(36) = 2.056870288868004;
    cool_coef(37) = 2.233680315878366;
    cool_coef(38) = 2.4097186710383474;
    cool_coef(39) = 2.5502102007949023;
    
    cool_index(0) = 5.455488390256632;
    cool_index(1) = 5.076170519863754;
    cool_index(2) = -0.5020655826640291;
    cool_index(3) = -1.7055659800651979;
    cool_index(4) = -0.5399688186820728;
    cool_index(5) = 0.550609170202909;
    cool_index(6) = 1.4527662908446985;
    cool_index(7) = 2.0172644735605223;
    cool_index(8) = 1.773197476674277;
    cool_index(9) = 0.6282445620956022;
    cool_index(10) = -0.2747076405016009;
    cool_index(11) = 0.07013182420220869;
    cool_index(12) = 0.32752568568776125;
    cool_index(13) = 0.1883881016798681;
    cool_index(14) = -1.6570303755459093;
    cool_index(15) = -1.209120245966656;
    cool_index(16) = -0.17533183860418153;
    cool_index(17) = -1.0512755674245657;
    cool_index(18) = -0.896664392554265;
    cool_index(19) = -0.16540667885641686;
    cool_index(20) = -0.43250361812273735;
    cool_index(21) = -0.4775539072045259;
    cool_index(22) = -1.0956186284443203;
    cool_index(23) = -1.453147878451421;
    cool_index(24) = -1.3412596915753237;
    cool_index(25) = -1.0051719479026813;
    cool_index(26) = -0.573142729390977;
    cool_index(27) = -0.3457087236213044;
    cool_index(28) = -0.08698732111048613;
    cool_index(29) = -0.07335511773234596;
    cool_index(30) = -0.3119882060952377;
    cool_index(31) = -0.6835132944311395;
    cool_index(32) = -0.6549261784681947;
    cool_index(33) = -0.2804886559029823;
    cool_index(34) = 0.05737205818565948;
    cool_index(35) = 0.30836313806582183;
    cool_index(36) = 0.3580735000106496;
    cool_index(37) = 0.3293929876114671;
    cool_index(38) = 0.24620665148692336;
    cool_index(39) = 0.10953503955831644;
    
    // Calculate All TEFs Y_k recursively (Eq. A6)
    cool_tef(nbins-1) = 0.0; // Last Y_N = 0
    for (int i=nbins-2; i>=0; i--) {
        Real t_n    = cool_t(nbins-1);
        Real coef_n = cool_coef(nbins-1);
        
        Real t_i   = cool_t(i);
        Real tef_i = cool_tef(i);
        Real coef  = cool_coef(i);
        Real slope = cool_index(i);
        
        Real sm1  = slope - 1.0;
        Real step = (coef_n/coef)*(t_i/t_n)*(pow(t_i/cool_t(i+1),sm1)-1)/sm1;
        cool_tef(i) = cool_tef(i+1) - step;
    }
    
    // Compute the constant factor needed to compute new TEFs
    Real g  = 5.0/3.0; // adiabatic index
    Real X = 0.7; Real Z = 0.02; // Fully ionized, solar abundances
    Real mu   = 1.0/(2.0*X + 0.75*(1.0-X-Z) + Z/2.0);
    Real mu_e = 2.0/(1.0+X);
    Real mu_h = 1.0/X;
    
    
    
    const_factor = (1.0e-23)*(g-1.0)*mu/(kb*mu_e*mu_h*mh);
    
    return;
}

void Mesh::UserWorkAfterLoop(ParameterInput *pin)
{
    std::cout << std::endl;
    std::cout << "CLOUD CRUSHING TIME (CU): " << cloudCrushingTime << std::endl;
    
    std::cout << "CLOUD CRUSHING TIME (CGS): " << t_cc_cgs() << std::endl;
    
    std::cout << "TCOOL,MIX (CGS): " << tCoolMix << std::endl;
    std::cout << "t_coolmix/t_cc: " << tCoolMix/t_cc_cgs() << std::endl;
    std::cout << "TCOOL,WIND/T_CC (IN CGS):" << t_cool(gridDensity, windTemperature)/t_cc_cgs() << std::endl;
    
    std::cout << "CLOUD DENSITY (CU): " << cloudDensity  << std::endl;
    std::cout << "CLOUD DENSITY (CGS): " << cloudDensity * unit_rho << std::endl;
    std::cout << "CLOUD PRESSURE (CU): " << cloudPressure << std::endl;
    std::cout << "CLOUD PRESSURE (CGS): " << cloudPressure * unit_pres << std::endl;
    std::cout << "CLOUD RADIUS (CU): " << rad << std::endl;
    
    std::cout << "TEMPERATURE CLOUD: " << cloudTemperature * unit_temp << std::endl;
    std::cout << "TEMPERATURE WIND: " << windTemperature * unit_temp << std::endl;

    Real gmma  = 5/3.0;

    speedOfSound = std::sqrt(gmma * gridPressure/gridDensity); //speed of sound is in code units
    
    std::cout << "SPEED OF SOUND: " << speedOfSound << std::endl;
    
    cool_t.DeleteAthenaArray();
    cool_tef.DeleteAthenaArray();
    cool_coef.DeleteAthenaArray();
    cool_index.DeleteAthenaArray();
    
    return;
}

void MeshBlock::InitUserMeshBlockData(ParameterInput *pin)
{
    AllocateUserOutputVariables(1);
}

Real ColdGasMass(MeshBlock *pmb, int iout)
{
    Real coldGasMass = 0;
    int is=pmb->is, ie=pmb->ie, js=pmb->js, je=pmb->je, ks=pmb->ks, ke=pmb->ke;
    
    for(int k=ks; k<=ke; k++)
        for(int j=js; j<=je; j++)
            for (int i=is; i<=ie; i++)
                if (pmb->phydro->u(IDN,k,j,i) > cloudDensity * coldGasFraction)
                    coldGasMass += pmb->phydro->u(IDN,k,j,i) * pmb->pcoord->GetCellVolume(k,j,i);
                
    return coldGasMass;
}

Real ColdGasMomentum(MeshBlock *pmb, int iout)
{
    Real coldGasMomentum = 0;
    for (int k=pmb->ks; k<=pmb->ke; ++k) {
        for (int j=pmb->js; j<=pmb->je; ++j) {
            for (int i=pmb->is; i<=pmb->ie; ++i) {
                if (pmb->phydro->u(IDN,k,j,i) > cloudDensity * coldGasFraction)
                {
                    coldGasMomentum += pmb->phydro->u(IM1,k,j,i) * pmb->pcoord->GetCellVolume(k,j,i);
                }
            }
        }
    }
    return coldGasMomentum; 
}


Real GridVelocityOut(MeshBlock *pmb, int iout)
{
    return gridVelocity;
}

