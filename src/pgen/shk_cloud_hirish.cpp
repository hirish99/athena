// C++ headers
#include <cmath>      // sqrt()
#include <iostream>   // endl
#include <sstream>    // stringstream
#include <stdexcept>  // runtime_error
#include <string>     // c_str()

// Athena++ headers
#include "../athena.hpp"
#include "../athena_arrays.hpp"
#include "../parameter_input.hpp"
#include "../bvals/bvals.hpp"
#include "../coordinates/coordinates.hpp"
#include "../eos/eos.hpp"
#include "../field/field.hpp"
#include "../hydro/hydro.hpp"
#include "../mesh/mesh.hpp"
#include "../utils/utils.hpp"

static Real gmma1;
static Real gridPressure, gridVelocity, gridDensity;
static Real coldGasFraction = 0.33;
static Real cloudDensity;
static Real cloudTemperature;
static Real windTemperature;

Real ColdGasMass(MeshBlock *pmb, int iout);

void ShockCloudInnerX1(MeshBlock *pmb, Coordinates *pco, AthenaArray<Real> &prim,
                       FaceField &b, Real time, Real dt,
                       int is, int ie, int js, int je, int ks, int ke, int ngh);

void Mesh::InitUserMeshData(ParameterInput *pin)
{
    AllocateUserHistoryOutput(1);
    EnrollUserHistoryOutput(0, ColdGasMass, "ColdGasMass");
    EnrollUserBoundaryFunction(INNER_X1, ShockCloudInnerX1);
}

void MeshBlock::ProblemGenerator(ParameterInput *pin) {
  Real gmma  = peos->GetGamma();
  gmma1 = gmma - 1.0;

  cloudTemperature = cloudPressure/cloudDensity;
  windTemperature = gridPressure/gridDensity;

  gridPressure = pin->GetReal("problem", "GP");
  gridVelocity = pin->GetReal("problem", "GV");
  gridDensity = pin->GetReal("problem", "GD");
  Real rad = pin->GetReal("problem", "CR");
  Real cloudPressure = pin->GetReal("problem", "CP");
  Real cloudVelocity = pin->GetReal("problem", "CV");
  cloudDensity = pin->GetReal("problem", "CD");

  // Initialize the grid
  for (int k=ks; k<=ke; k++) {
  for (int j=js; j<=je; j++) {
  for (int i=is; i<=ie; i++) {

      phydro->u(IDN,k,j,i) = gridDensity;
      phydro->u(IM1,k,j,i) = gridVelocity*gridDensity;
      phydro->u(IM2,k,j,i) = 0.0;
      phydro->u(IM3,k,j,i) = 0.0;
      phydro->u(IEN,k,j,i) = gridPressure/(gmma1) + 0.5 * gridDensity * (gridVelocity * gridVelocity);
      
      Real expFactor = exp(grav * pcoord->x1v(i)/windTemperature);
      
      Real diag = std::sqrt(SQR(pcoord->x1v(i)) + SQR(pcoord->x2v(j))
                            + SQR(pcoord->x3v(k)));
      if (diag < rad) {
          phydro->u(IDN,k,j,i) = cloudDensity * expFactor;
          phydro->u(IM1,k,j,i) = cloudVelocity*cloudDensity * expFactor;
          phydro->u(IM2,k,j,i) = 0.0;
          phydro->u(IM3,k,j,i) = 0.0;
          phydro->u(IEN,k,j,i) = expFactor * cloudPressure/(gmma1) + expFactor * 0.5 * cloudDensity * (cloudVelocity*cloudVelocity);
      }

  }}}

    return;
}

void ShockCloudInnerX1(MeshBlock *pmb, Coordinates *pco, AthenaArray<Real> &prim,
                       FaceField &b, Real time, Real dt,
                       int is, int ie, int js, int je, int ks, int ke, int ngh) {
  for (int k=ks; k<=ke; ++k) {
  for (int j=js; j<=je; ++j) {
    for (int i=1; i<=ngh; ++i) {
    
      Real expFactor = exp(grav * pcoord->x1v(i)/windTemperature);
      
      Real diag = std::sqrt(SQR(pcoord->x1v(i)) + SQR(pcoord->x2v(j))
                            + SQR(pcoord->x3v(k)));
    
      prim(IDN,k,j,is-i) = gridDensity * expFactor;
      prim(IVX,k,j,is-i) = gridVelocity * expFactor;
      prim(IVY,k,j,is-i) = 0.0;
      prim(IVZ,k,j,is-i) = 0.0;
      prim(IPR,k,j,is-i) = gridPressure * expFactor;
    }
  }}
}

Real ColdGasMass(MeshBlock *pmb, int iout)
{
    Real coldGasMass = 0;
    int is=pmb->is, ie=pmb->ie, js=pmb->js, je=pmb->je, ks=pmb->ks, ke=pmb->ke;
    
    for(int k=ks; k<=ke; k++)
        for(int j=js; j<=je; j++)
            for (int i=is; i<=ie; i++)
                if (pmb->phydro->u(IDN,k,j,i) > cloudDensity * coldGasFraction)
                    coldGasMass += pmb->phydro->u(IDN,k,j,i) * pmb->pcoord->GetCellVolume(k,j,i);
    
    return coldGasMass;
}
