cool_t = [0 for i in range(0, 40)]
cool_coef = [0 for i in range(0, 40)]
cool_index = [0 for i in range(0, 40)]

cool_t[0] = 310000.0;
cool_t[1] = 12600.0;
cool_t[2] = 15890.0;
cool_t[3] = 20020.0;
cool_t[4] = 25240.0;
cool_t[5] = 31810.0;
cool_t[6] = 40090.0;
cool_t[7] = 50530.0;
cool_t[8] = 63680.0;
cool_t[9] = 80260.0;
cool_t[10] = 101200.0;
cool_t[11] = 127500.0;
cool_t[12] = 160700.0;
cool_t[13] = 202600.0;
cool_t[14] = 255300.0;
cool_t[15] = 321800.0;
cool_t[16] = 405600.0;
cool_t[17] = 511100.0;
cool_t[18] = 644200.0;
cool_t[19] = 812000.0;
cool_t[20] = 1000000.0;
cool_t[21] = 1259000.0;
cool_t[22] = 1585000.0;
cool_t[23] = 1995000.0;
cool_t[24] = 2512000.0;
cool_t[25] = 3162000.0;
cool_t[26] = 3981000.0;
cool_t[27] = 5012000.0;
cool_t[28] = 6310000.0;
cool_t[29] = 7943000.0;
cool_t[30] = 10000000.0;
cool_t[31] = 12590000.0;
cool_t[32] = 15850000.0;
cool_t[33] = 19950000.0;
cool_t[34] = 25120000.0;
cool_t[35] = 31620000.0;
cool_t[36] = 39810000.0;
cool_t[37] = 50120000.0;
cool_t[38] = 63100000.0;
cool_t[39] = 79430000.0;


cool_coef[0] = 1.6408984689285624;
cool_coef[1] = 5.789646575948292;
cool_coef[2] = 18.797203755396648;
cool_coef[3] = 16.7384754689852;
cool_coef[4] = 11.274384717759935;
cool_coef[5] = 9.95038422958871;
cool_coef[6] = 11.302144847043829;
cool_coef[7] = 15.819149070534786;
cool_coef[8] = 25.224636283348048;
cool_coef[9] = 38.02107089248533;
cool_coef[10] = 43.98219098299675;
cool_coef[11] = 41.277704007796586;
cool_coef[12] = 41.95311185975414;
cool_coef[13] = 45.260670345801;
cool_coef[14] = 47.275626188961176;
cool_coef[15] = 32.21420131907784;
cool_coef[16] = 24.350976818250636;
cool_coef[17] = 23.383616480583676;
cool_coef[18] = 18.333394532081098;
cool_coef[19] = 14.89691888284402;
cool_coef[20] = 14.392505898454834;
cool_coef[21] = 13.027915287005817;
cool_coef[22] = 11.671262753284271;
cool_coef[23] = 9.070904785425046;
cool_coef[24] = 6.489695397654223;
cool_coef[25] = 4.766239129792971;
cool_coef[26] = 3.7811870710765074;
cool_coef[27] = 3.313622783657129;
cool_coef[28] = 3.0600313080475674;
cool_coef[29] = 2.9993768457216112;
cool_coef[30] = 2.9491332141250552;
cool_coef[31] = 2.744653611808266;
cool_coef[32] = 2.3449511265716;
cool_coef[33] = 2.0169621177549892;
cool_coef[34] = 1.8907205849384978;
cool_coef[35] = 1.91584885606706;
cool_coef[36] = 2.056870288868004;
cool_coef[37] = 2.233680315878366;
cool_coef[38] = 2.4097186710383474;
cool_coef[39] = 2.5502102007949023;

cool_index[0] = 5.455488390256632;
cool_index[1] = 5.076170519863754;
cool_index[2] = -0.5020655826640291;
cool_index[3] = -1.7055659800651979;
cool_index[4] = -0.5399688186820728;
cool_index[5] = 0.550609170202909;
cool_index[6] = 1.4527662908446985;
cool_index[7] = 2.0172644735605223;
cool_index[8] = 1.773197476674277;
cool_index[9] = 0.6282445620956022;
cool_index[10] = -0.2747076405016009;
cool_index[11] = 0.07013182420220869;
cool_index[12] = 0.32752568568776125;
cool_index[13] = 0.1883881016798681;
cool_index[14] = -1.6570303755459093;
cool_index[15] = -1.209120245966656;
cool_index[16] = -0.17533183860418153;
cool_index[17] = -1.0512755674245657;
cool_index[18] = -0.896664392554265;
cool_index[19] = -0.16540667885641686;
cool_index[20] = -0.43250361812273735;
cool_index[21] = -0.4775539072045259;
cool_index[22] = -1.0956186284443203;
cool_index[23] = -1.453147878451421;
cool_index[24] = -1.3412596915753237;
cool_index[25] = -1.0051719479026813;
cool_index[26] = -0.573142729390977;
cool_index[27] = -0.3457087236213044;
cool_index[28] = -0.08698732111048613;
cool_index[29] = -0.07335511773234596;
cool_index[30] = -0.3119882060952377;
cool_index[31] = -0.6835132944311395;
cool_index[32] = -0.6549261784681947;
cool_index[33] = -0.2804886559029823;
cool_index[34] = 0.05737205818565948;
cool_index[35] = 0.30836313806582183;
cool_index[36] = 0.3580735000106496;
cool_index[37] = 0.3293929876114671;
cool_index[38] = 0.24620665148692336;
cool_index[39] = 0.10953503955831644;

import os
import math

mh        = 1.6605e-24
kb        = 1.380648e-16

unit_len  = 3.086e20
unit_temp = 1.0e6
unit_n    = 1.6e-4

unit_rho  = unit_n * mh
unit_pres = kb * unit_n * unit_temp
unit_vel  = (unit_pres / unit_rho)**0.5
unit_time = unit_len / unit_vel
unit_mag  = (4*math.pi*unit_pres)**0.5

restart = 1
sup = 0
mpi = 0
node = 0
nx1 = 0
nx2 = 0
nx3 = 0
meshnx1 = 0
meshnx2 = 0
meshnx3 = 0
track = 0
cooling = 0
name = ""
windSpeed = 0
radius = 0
gridDensity = 0
cloudDensity = 0
cloudPressure = 0
gridPressure = 0
x1max = 0
x1min = 0
x2max = 0
x2min = 0
x3max = 0
x3min = 0
alpha = 0
overdensity = 0
t_ccval = 0
t_coolmixval = 0
x1increase = 0
twod = 0
gmma = 5/3
coolingfloor = 0
heatingfloor = 0
mh = 1.6605e-24;

sup = eval(input("Supercomputer? (0=No, 1=Yes): "))        
track = eval(input("Track Cloud? (0=No, 1=Yes): "))
twod = eval(input("2D Sim? (0=No, 1=Yes): "))
if track == 1:
    print("Note that tracking means cooling is on.")
#x1increase = eval(input("By what factor to increase the right_x1 bounds (enter an integer, 0 means no change): "))
if track == 0:
    cooling = eval(input("Have Cooling? (0=No, 1=Yes): "))
#x1increase = eval(input("By what factor to increase the right_x1 bounds (enter an integer, 0 means no change): "))

while restart == 1:
    overdensity = eval(input("Enter the overdensity of the cloud: "))
    cloudTemperature = eval(input("Enter the temperature of the cloud in Kelvin: "))
    #windSpeed = eval(input("Enter the mach number of the wind: "))
    print("Automatically setting the mach number to 1.")
    windSpeed = 1
    
    radius = eval(input("Enter the radius of the cloud in code units: "))
    
    
    x1min = eval(input("Enter x1min in radius (< 0): "))*radius
    x1max = eval(input("Enter x1max in radius: "))*radius

    x2min = eval(input("Enter x2min/x3min in radius: "))*radius
    x2max = eval(input("Enter x2max/x3max in radius: "))*radius
    
    x3min = x2min
    x3max = x2max
    
    gridDensity = eval(input("Enter the wind density in code units (default=1): "))

    gmma = 5/3
    gmma1 = gmma - 1
    kb = 1.380648e-16;
    

    gridTemperature = cloudTemperature * overdensity   #NOT IN CODE UNITS : cloud/gridTemperature
    cloudDensity = overdensity * gridDensity
    cloudPressure = (cloudTemperature/unit_temp) * cloudDensity
    gridPressure = (gridTemperature/unit_temp) * gridDensity
    
    #grav = eval(input("Enter a gravitational acceleration: "))
    speedofsound = (gmma * gridPressure/gridDensity)**0.5
    
    C = 0
    if twod == 1:
        C = 1.76
    else:
        C = 0.6
    
    grav = -((windSpeed*speedofsound)**2) * 3 * C/(8*radius*overdensity) #in code units
    
    vterm = ((8*radius*overdensity*(-grav))/(3*C))**0.5 #in code units
    
    print("The gravitational acceleration is being set to", grav, "since this leads to a terminal velocity of", vterm/speedofsound, "mach")
    

    
    

    def t_cc():
        return (math.sqrt(cloudDensity/gridDensity) * radius/((windSpeed * (gmma * gridPressure/gridDensity)**0.5)))
    
    def t_cc_cgs():
        return (math.sqrt(cloudDensity/gridDensity) * (radius*unit_len)/((windSpeed * (gmma * gridPressure/gridDensity)**0.5)*unit_vel))
    

    def Lambda(T):
        index = 0
        for i in range(0, 40):
            if T > cool_t[i]:
                index+=1
        if index != 0:
            index -= 1
        return 10**(-23) * cool_coef[index] * (T / cool_t[index]) ** (cool_index[index])

    X = 0.7
    Z = 0.02
    mu   = 1.0/(2.0*X + 0.75*(1.0-X-Z) + Z/2.0)
    mu_e = 2.0/(1.0+X)
    mu_h = 1.0/X
    

    #INPUTS IN CGS, OUTPUTS IN CGS
    def t_cool(rho, T):
        return (kb * mu_e * mu_h * mh * T) / (gmma1 * (rho) * mu * Lambda(T))

    def t_coolmix():
        return (t_cool(math.sqrt(cloudDensity * gridDensity)*unit_rho, math.sqrt(cloudTemperature * gridTemperature)))

    #NOTE THAT BOTH ARE IN CODE UNITS
    t_ccval = t_cc()
    t_coolmixval = t_coolmix()

    alpha = str(t_coolmix()/t_cc_cgs())
    print("t_cc:"+str(t_ccval)+";t_cc_cgs:"+str(t_cc_cgs())+";t_cool,mix(cgs):"+str(t_coolmixval))
    restart = eval(input("Your chosen inputs have led to a t_cool,mix/t_cc of: " + alpha + ". Restart? (0=No, 1=Yes): "))


resol = 0

coolingfloor = eval(input("Enter a cooling floor fraction: "))
heatingfloor = eval(input("Enter a heating floor fraction: "))


if sup == 1:
    proceed = 0
    while proceed == 0:
        resol = eval(input("Enter a d_cell/r_cl: "))
        nx1 = (resol)*(((x1max-x1min)/radius)//1)
        nx2 = (resol)*(((x2max-x2min)/radius)//1)
        nx3 = (resol)*(((x3max-x3min)/radius)//1)
        if twod == 1:
            nx3 = 1
        print("This means nx1 is " + str(nx1) + ", nx2 is " + str(nx2) + ", nx3 is " + str(nx3) + ".")
        meshnx1 = eval(input("Enter decomp in X1-direction: "))
        meshnx2 = eval(input("Enter decomp in X2-direction: "))
        meshnx3 = eval(input("Enter decomp in X3-direction: "))
        mpi = nx1/meshnx1 * nx2/meshnx2 * nx3/meshnx3
        proceed = eval(input("This means " + str(mpi) + " mpi tasks. Proceed? (0=ChangeDecomp,1=Proceed): "))
    node = eval(input("Enter the number of nodes: "))
else:
    proceed = 0
    while proceed == 0:
        resol = eval(input("Enter a d_cell/r_cl: "))
        nx1 = (resol)*(((x1max-x1min)/radius)//1)
        nx2 = (resol)*(((x2max-x2min)/radius)//1)
        nx3 = (resol)*(((x3max-x3min)/radius)//1)
        if twod == 1:
            nx3 = 1
        proceed = eval(input("This means nx1 is " + str(nx1) + ", nx2 is " + str(nx2) + ", nx3 is " + str(nx3) + ". Proceed? (0=Change,1=Proceed): "))


hstoutdt = eval(input("Enter the dt of .hst output in t_cc: ")) * t_ccval
vtkoutdt = eval(input("Enter the dt of .hdf5 output in t_cc: ")) * t_ccval

timetotrust = (x1max - x1min) / (windSpeed * (gmma * gridPressure/gridDensity)**0.5)

tlim = eval(input("Enter the amount of t_cc to simulate (trust for "+str(timetotrust/t_ccval)+"t_cc): ")) * t_ccval

restartfile = eval(input("Enter the dt for a restart file in t_cc or enter 0 for no restart file: ")) * t_ccval


if tlim > timetotrust:
    print("Wind Travels through Box in:", timetotrust/t_ccval, "t_cc, please change the time to run:")
    tlim = eval(input("Enter the amount of t_cc to simulate: ")) * t_ccval

name = input("Enter a name for this run: ")


if sup == 0:
    print("An input file is about to be created in the directory ./" + name + " along with associated .hdf5 files and plots.")
else:
    print("An input file, a job script, and associated .hdf5 files and plots will be created in ./" + name + " and will then be run.")


if eval(input("Do you want to make clean? (0=No, 1=Yes): ")) == 1:
    os.system("make clean")

if twod == 1:
    nx3 = 1

os.system("mkdir " + name)
f = open("./"+name+"/"+"athinput.in","w+")
f.write("<job>\n")
f.write("problem_id="+name+"\n")
f.write("<output1>\n")
f.write("file_type=hst\n")
f.write("dt=" + str(hstoutdt)+"\n")

if sup == 0:
    f.write("<output2>\n")
    f.write("file_type=vtk\n")
    f.write("id=prim\n")
    f.write("variable=prim\n")
    f.write("ghost_zones = 1\n")
    f.write("dt=" + str(vtkoutdt)+"\n")
else:
    f.write("<output2>\n")
    f.write("file_type=hdf5\n")
    f.write("id=prim\n")
    f.write("variable=prim\n")
    f.write("dt=" + str(vtkoutdt)+"\n")

if restartfile != 0:
    f.write("<output3>\n")
    f.write("file_type=rst\n")
    f.write("dt=" + str(restartfile)+"\n")

f.write("<time>\n")
f.write("cfl_number=0.4\n")
f.write("nlim=10000000000000\n")
f.write("tlim="+str(tlim//1)+"\n")
f.write("integrator=vl2\n")
f.write("xorder=2\n")
f.write("ncycle_out=1\n")
f.write("<mesh>\n")
f.write("nx1="+str(nx1)+"\n")
f.write("nx2="+str(nx2)+"\n")
f.write("nx3="+str(nx3)+"\n")
f.write("x1min="+str(x1min)+"\n")
f.write("x1max="+str(x1max)+"\n")
f.write("ix1_bc=user\n")
f.write("ox1_bc=user\n")
f.write("x2min="+str(x2min)+"\n")
f.write("x2max="+str(x2max)+"\n")
f.write("ix2_bc=outflow\n")
f.write("ox2_bc=outflow\n")
f.write("x3min="+str(x3min)+"\n")
f.write("x3max="+str(x3max)+"\n")
f.write("ix3_bc=outflow\n")
f.write("ox3_bc=outflow\n")

if mpi != 0:
    f.write("<meshblock>\n")
    f.write("nx1="+str(meshnx1)+"\n")
    f.write("nx2="+str(meshnx2)+"\n")
    f.write("nx3="+str(meshnx3)+"\n")

f.write("<hydro>\n")
f.write("iso_sound_speed=1.0\n")
f.write("gamma=1.66667\n")
f.write("grav_acc1="+str(grav)+"\n")
f.write("<problem>\n")
f.write("GP="+str(gridPressure)+"\n")
f.write("GD="+str(gridDensity)+"\n")
f.write("GV="+str(windSpeed)+"\n")
f.write("CR="+str(radius)+"\n")
f.write("CP="+str(cloudPressure)+"\n")
f.write("CV=0\n")
f.write("CD="+str(cloudDensity)+"\n")
f.write("CF="+str(coolingfloor)+"\n")
f.write("HF="+str(heatingfloor)+"\n")

f.close()

if sup == 0 and track == 1:
    os.system("python3 configure.py --prob shk_cloud_hirish_wcooling_comoving_synced -hdf5")
    os.system("make all")
    os.system("bin/athena -i ./"+name+"/"+"athinput.in")
    os.system("mv *"+name+".athdf ./"+name)
    os.system("mv *"+name+".athdf.xdmf ./"+name)
    os.system("mv "+name+"*.vtk ./"+name)
    os.system("mv "+name+"*.hst ./"+name)

if sup == 0 and track == 0 and cooling == 1:
    os.system("python3 configure.py --prob shk_cloud_hirish_wcooling -hdf5")
    os.system("make all")
    os.system("bin/athena -i ./"+name+"/"+"athinput.in")
    os.system("mv *"+name+".athdf ./"+name)
    os.system("mv *"+name+".athdf.xdmf ./"+name)
    os.system("mv "+name+"*.vtk ./"+name)
    os.system("mv "+name+".hst ./"+name)

if sup == 0 and track == 0 and cooling == 0:
    os.system("python3 configure.py --prob shk_cloud_hirish -hdf5")
    os.system("make all")
    os.system("bin/athena -i ./"+name+"/"+"athinput.in")
    os.system("mv *"+name+".athdf ./"+name)
    os.system("mv *"+name+".athdf.xdmf ./"+name)
    os.system("mv "+name+"*.vtk ./"+name)
    os.system("mv "+name+".hst ./"+name)

if sup == 1:
    queue = input("normal or development queue?: ")
    tlimit = input("enter a time limit: ")
    f = open("./"+"jobscript","w+")
    f.write("#!/bin/bash\n")
    f.write("#SBATCH -J "+name+"\n")
    f.write("#SBATCH -o "+name+".o%j\n")
    f.write("#SBATCH -e "+name+".e%j\n")
    f.write("#SBATCH -p "+queue+"\n")
    f.write("#SBATCH -N " + str(node) + "\n")
    f.write("#SBATCH -n " + str(int(mpi//1)) + "\n")
    f.write("#SBATCH -t "+tlimit+"\n")
    f.write("#SBATCH --mail-user=hirish@ucsb.edu\n")
    f.write("#SBATCH --mail-type=all\n")
    f.write("module load intel\n")
    f.write("module load phdf5\n")
    if track == 1:
        f.write("python configure.py --prob=shk_cloud_hirish_wcooling_comoving_synced -mpi --cxx=icc-phi -hdf5 -h5double --include=$TACC_HDF5_INC --lib=$TACC_HDF5_LIB\n")
        f.write("make all\n")
        f.write("mpirun -np "+str(int(mpi//1))+" ./bin/athena -i ./"+name+"/"+"athinput.in")
    if track == 0 and cooling == 1:
        f.write("python configure.py --prob=shk_cloud_hirish_wcooling -mpi --cxx=icc-phi -hdf5 -h5double --include=$TACC_HDF5_INC --lib=$TACC_HDF5_LIB\n")
        f.write("make all\n")
        f.write("mpirun -np "+str(int(mpi//1))+" ./bin/athena -i ./"+name+"/"+"athinput.in")
    if track == 0 and cooling == 0:
        f.write("python configure.py --prob=shk_cloud_hirish -mpi --cxx=icc-phi -hdf5 -h5double --include=$TACC_HDF5_INC --lib=$TACC_HDF5_LIB\n")
        f.write("make all\n")
        f.write("mpirun -np "+str(int(mpi//1))+" ./bin/athena -i ./"+name+"/"+"athinput.in")
    f.close()

    os.system("sbatch ./jobscript")

print("import numpy as np")
print("import yt")
print("import h5py")
print("import matplotlib.pyplot as plt")
print('ds = yt.load("./out.prim.00000.athdf")')
print('slc = yt.SlicePlot(ds, "z", ["density"])')
print('slc.set_cmap("density", "Blue-Red")')
print("slc.annotate_grids(cmap=None)")
print("slc.show()")
print('dat = np.loadtxt("./out.hst")')
print("plt.plot(dat[:,0]/"+str(t_ccval)+", dat[:,10]/dat[0,10], label = 'Fraction of Cold Gas Mass')")
print("plt.plot(dat[:,0]/"+str(t_ccval)+", dat[:,12]-dat[:,11]/dat[:,10], label = 'Velocity of Cold Gas Mass')")
print('plt.xlabel("Time in t_cc")')
print('plt.title("t_cool,mix/t_cc: '+str(alpha)+';ws:'+str(windSpeed*(gmma*gridPressure/gridDensity)**0.5)+';cT:'+str(cloudTemperature)+';trust:'+str(timetotrust/t_ccval)+'t_cc;grav:'+str(grav)+'")')
print("plt.show()")

    
    
    
