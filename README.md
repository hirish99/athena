athena
======
[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

```
box setup with gravitational acceleration leftward, cloud positioned rightward. cooling not enrolled.  
<img src="./hydrostatictest2.prim.00000_Projection_z_density.jpg">
``` 
